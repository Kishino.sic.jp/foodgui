import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton karaageButton;
    private JButton gyozaButton;
    private JButton yakisobaButton;
    private JButton checkOutButton;
    private JLabel topLabel;
    private JLabel OrderedItemsLabel;
    private JTextPane OrderedItems;
    private JLabel TotalMoney;
    String CurrentText;
    int total=0;
    int confirmation;

    void order(String food) {
        confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            CurrentText = OrderedItems.getText();
            OrderedItems.setText(CurrentText + "\n" + food);
            total += 100;
            TotalMoney.setText("Total       " + total + "yen");
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering " + food + "!  It will be served as soon as possible.");
        }
    }

    public FoodGUI() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage");
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza");
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                confirmation=JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirm",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0){
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is " + total + "yen.");
                    total=0;
                    OrderedItems.setText("");
                    TotalMoney.setText("Total       "+ total +"yen");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}